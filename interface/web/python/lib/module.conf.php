<?php
/**
 * Created by PhpStorm.
 * User: Stefan <info@corebyte.nl>
 * Date: 31-01-15
 * Time: 16:36
 */

//**** Module Definition ****

// Name of the module. The module name must match the name of the module directory.
// The module name may not contain spaces.
$module['name']      = 'python';

// Title of the module which is dispalayed in the top navigation.
$module['title']     = 'python';

// The template file of the module. This is always 'module.tpl.htm' unless
// there are any special requirements such as a three column layout.
$module['template']  = 'module.tpl.htm';

// The page that is displayed when the module is loaded.
// The path must is relative to the web/ directory
$module['startpage'] = 'python/websites_list.php';

// The width of the tab. Normally you should leave this empty and
// let the browser define the width automatically.
$module['tab_width'] = '';

//****  Menu Definition ****

// Make sure that the items array is empty
$items = array();

// Add a menu item with the label 'Websites'
$items[] = array( 'title'   => 'Websites',
    'target'  => 'content',
    'link'    => 'python/websites_list.php?type=domain',
    'html_id'   => 'sites_list');

// Add a menu item with the label 'Cron jobs'
$items[] = array( 'title'   => 'Cron jobs',
    'target'  => 'content',
    'link'    => 'python/cron_jobs.php',
    'html_id'   => 'cron_job_list');

// Add a menu item with the label 'Task scheduler'
$items[] = array( 'title'   => 'Task scheduler',
    'target'  => 'content',
    'link'    => 'python/task_scheduler.php',
    'html_id'   => 'task_scheduler_list');

// Append the menu $items defined above to a menu section labeled 'web2py websites'
$module['nav'][] = array( 'title' => 'Web2py websites',
    'open'  => 1,
    'items'	=> $items
);

?>